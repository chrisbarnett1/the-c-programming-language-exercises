#include <stdio.h>

#define LOWER 0   /* lower limit of table */
#define UPPER 300 /* upper limit */
#define STEP  20  /* step size */

/* print Celsius-Fahrenheit table */
main()
{
	int celsius;

	printf("C    F\n");
	for (celsius = LOWER; celsius <= UPPER; celsius = celsius + STEP)
		printf("%3d %6.1f\n", celsius, (9.0/5.0) * celsius + 32.0);
}
